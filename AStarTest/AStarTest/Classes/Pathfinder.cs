﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace AStarTest
{
    class Pathfinder
    {
        //Lists to determine nodes
        List<NodeCell> nodeCells = new List<NodeCell>();
        List<NodeCell> openList = new List<NodeCell>();
        List<NodeCell> closedList = new List<NodeCell>();
        List<NodeCell> pathway = new List<NodeCell>();

        NodeCell currentNodeCell;
        int startNodeID;
        int mapSizeX = 5;
        int mapSizeZ = 5;
        
        //Sets the path (Access via getPath())
        public void pathFind(NodeCell nodeStart, NodeCell nodeEnd)
        {
            NodeCell goalNode = new NodeCell(nodeEnd.getNodeID());
            NodeCell startNode = new NodeCell(nodeStart.getNodeID());
            //int lastHValue = findManhattanDistance(startNode, goalNode);

            startNode.updateDistance(0, findManhattanDistance(startNode, goalNode));
            //Console.WriteLine("ManhattanDistance from start: " + startNode.getDistance());

            resetValues();
            openList.Add(startNode);

            while (openList.Count > 0)
            {
                currentNodeCell = findBestNode(openList); //Get node with lowest f Value
                //if (currentNodeCell != null) //Nodepath exists
                //{
                if (currentNodeCell.getNodeID() == goalNode.getNodeID())
                {
                    //Reached, get path by searching through parent nodes of last child
                    setPath(currentNodeCell);
                    break;
                }
                if (openList.Count > 1)
                    openList = sortNodes(openList);
                openList.RemoveAt(0);
                closedList.Add(currentNodeCell); // Add node to confirmed row

                List<NodeCell> neighbours = getNeighbours(currentNodeCell);
                foreach (NodeCell node in neighbours)
                {
                    float g = currentNodeCell.g() + (1 * node.getNodeValue());
                    float h = findManhattanDistance(node, goalNode);
                    float f = g + h;

                    if (closedList.Contains(node) && f >= node.f()) //If confirmed path has node and f value higher than current
                    {
                        continue; //Skips the loop
                    }

                    if (!openList.Contains(node) || f < node.f()) //If open list does not have node or totalDistance is less than current
                    {
                        node.setParent(currentNodeCell);
                        node.updateDistance((int)g, (int)h);
                        if (!openList.Contains(node)) //If open list does not have node ONLY
                        {
                            //add to list
                            node.setMark(true);
                            openList.Add(node);
                        }
                    }
                }
                //}
                //else //If path does not exist
                //{
                //    pathway = new List<NodeCell>();
                //    break;
                //}
            }
        }

        public List<NodeCell> getPossibleMoveLocations(NodeCell nodeStart, Character playerUnit)
        {
            List<NodeCell> possibleMoveLocations = new List<NodeCell>();
            NodeCell startNode = new NodeCell(nodeStart.getNodeID());

            startNode.updateDistance(0, 0);
            //Console.WriteLine("ManhattanDistance from start: " + startNode.getDistance());

            resetValues();
            List<NodeCell> openListLoc = new List<NodeCell>();
            List<NodeCell> closedListLoc = new List<NodeCell>();
            openListLoc.Add(startNode);

            while (openListLoc.Count > 0)
            {
                currentNodeCell = findBestNode(openListLoc); //Get node with lowest f Value
                //if (currentNodeCell != null) //Nodepath exists
                //{
                if (currentNodeCell.getDistance() > playerUnit.getAPValue() * 2)
                {
                    break;
                }
                if (openListLoc.Count > 1)
                    openListLoc = sortNodes(openListLoc);
                openListLoc.RemoveAt(0);
                closedListLoc.Add(currentNodeCell); // Add node to confirmed row

                List<NodeCell> neighbours = getNeighbours(currentNodeCell);
                foreach (NodeCell node in neighbours)
                {
                    float g = currentNodeCell.g() + (1 * node.getNodeValue());
                    float h = 0;
                    float f = g + h;

                    if (closedListLoc.Contains(node) && f >= node.f()) //If confirmed path has node and f value higher than current
                    {
                        continue; //Skips the loop
                    }

                    if (!openListLoc.Contains(node) || f < node.f()) //If open list does not have node or totalDistance is less than current
                    {
                        node.setParent(currentNodeCell);
                        node.updateDistance((int)g, (int)h);
                        if (!openListLoc.Contains(node)) //If open list does not have node ONLY
                        {
                            //add to list
                            node.setMark(true);
                            openListLoc.Add(node);
                        }
                    }
                }
            }

            for (int i = 0; i < nodeCells.Count; i++)
            {
                nodeCells[i].setMark(false);
            }

            possibleMoveLocations = closedListLoc;
            return possibleMoveLocations;
        }

        public List<NodeCell> getPossibleAttackLocations(NodeCell nodeStart, Character playerUnit)
        {
            List<NodeCell> possibleAttackLocations = new List<NodeCell>();
            NodeCell startNode = new NodeCell(nodeStart.getNodeID());

            startNode.updateDistance(0, 0);
            //Console.WriteLine("ManhattanDistance from start: " + startNode.getDistance());

            resetValues();
            List<NodeCell> openListLoc = new List<NodeCell>();
            List<NodeCell> closedListLoc = new List<NodeCell>();
            openListLoc.Add(startNode);

            while (openListLoc.Count > 0)
            {
                currentNodeCell = findBestNode(openListLoc); //Get node with lowest f Value
                //if (currentNodeCell != null) //Nodepath exists
                //{
                if (currentNodeCell.getDistance() > (playerUnit.getAPValue() - 1) * 2) //Cannot move max distance and attack
                {
                    break;
                }
                if (openListLoc.Count > 1)
                    openListLoc = sortNodes(openListLoc);
                openListLoc.RemoveAt(0);
                closedListLoc.Add(currentNodeCell); // Add node to confirmed row

                List<NodeCell> neighbours = getNeighbours(currentNodeCell);
                foreach (NodeCell node in neighbours)
                {
                    float g = currentNodeCell.g() + (1 * node.getNodeValue());
                    float h = 0;
                    float f = g + h;

                    if (closedListLoc.Contains(node) && f >= node.f()) //If confirmed path has node and f value higher than current
                    {
                        continue; //Skips the loop
                    }

                    if (!openListLoc.Contains(node) || f < node.f()) //If open list does not have node or totalDistance is less than current
                    {
                        node.setParent(currentNodeCell);
                        node.updateDistance((int)g, (int)h);
                        if (!openListLoc.Contains(node)) //If open list does not have node ONLY
                        {
                            //add to list
                            node.setMark(true);
                            openListLoc.Add(node);
                        }
                    }
                }
            }

            for (int i = 0; i < nodeCells.Count; i++)
            {
                nodeCells[i].setMark(false);
            }

            //possibleAttackLocations = closedListLoc;
            for (int i = 0; i < closedListLoc.Count; i++) //For every block within movement range
            {
                List<NodeCell> neighbours = getNeighbours(closedListLoc[i], true); // Get neighbour
                for (int j = 0; j < neighbours.Count; j++)
                {
                    //If neighbour is not in list, but cell is occupied
                    if (neighbours[j].getOccupied() == true && possibleAttackLocations.Contains(neighbours[j]) == false)
                    {
                        possibleAttackLocations.Add(neighbours[j]);
                    }
                }
            }

            possibleAttackLocations.Remove(nodeCells[nodeStart.getNodeID()]);
            return possibleAttackLocations;
        }

        //Finds best node within list (with the lowest movement cost value)
        public NodeCell findBestNode(List<NodeCell> openList)
        {
            List<NodeCell> pathNodes = openList;

            int lowestDistanceNodeID = -1;
            int lowestDistanceTotal = 10000;
            for (int i = 0; i < pathNodes.Count; i++)
            {
                if (pathNodes[i].getDistance() < lowestDistanceTotal)
                {
                    lowestDistanceTotal = pathNodes[i].getDistance();
                    lowestDistanceNodeID = pathNodes[i].getNodeID();
                }
            }

            if (lowestDistanceNodeID == -1)
            {
                return null;
            }
            else
            {
                //Console.WriteLine("NodeID : " + lowestDistanceNodeID);
                return nodeCells[lowestDistanceNodeID];
            }
        }

        //Get neighbours of current node. Auto-detects and ignore blocked nodes
        public List<NodeCell> getNeighbours(NodeCell currentNode)
        {
            List<NodeCell> pathNodes = new List<NodeCell>();

            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if (i == 0 && j == 0) //If not center
                    {
                        continue;
                    }
                    if (i != j && i != -j) //If not diagonals
                    {
                        if (!(currentNode.getCoordinates().X == 0 && i < 0) && //If NOT at left-est row and moves left
                            !(currentNode.getCoordinates().X == (mapSizeX -1) && i > 0) && // If NOT at right-est row and moves right
                            !(currentNode.getCoordinates().Y == 0 && j < 0) && //If NOT at top row and moves up
                            !(currentNode.getCoordinates().Y == (mapSizeZ -1) && j > 0)) //If NOT at btm row and moves down
                        {
                            int newNeighbourID = currentNode.getNodeID() + i + (j * mapSizeZ);
                            if (nodeCells[newNeighbourID].getOccupied() == false //If not occupied
                                && nodeCells[newNeighbourID].getMark() == false) //If not marked
                            {
                                pathNodes.Add(nodeCells[newNeighbourID]);
                            }
                        }
                    }
                }
            }
            return pathNodes;
        }

        //Get neighbours. If ignoreOccupied, gets all neighbours, 
        //if !ignoreOccupied, gets only neighbours that are not occupied
        public List<NodeCell> getNeighbours(NodeCell currentNode, bool ignoreOccupied)
        {
            List<NodeCell> pathNodes = new List<NodeCell>();

            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if (i == 0 && j == 0) //If not center
                    {
                        continue;
                    }
                    if (i != j && i != -j) //If not diagonals
                    {
                        if (!(currentNode.getCoordinates().X == 0 && i < 0) && //If NOT at left-est row and moves left
                            !(currentNode.getCoordinates().X == (mapSizeX - 1) && i > 0) && // If NOT at right-est row and moves right
                            !(currentNode.getCoordinates().Y == 0 && j < 0) && //If NOT at top row and moves up
                            !(currentNode.getCoordinates().Y == (mapSizeZ - 1) && j > 0)) //If NOT at btm row and moves down
                        {
                            int newNeighbourID = currentNode.getNodeID() + i + (j * mapSizeZ);
                            //If not ignoring, check occupied. If ignoring, move on
                            if ((nodeCells[newNeighbourID].getOccupied() == false && ignoreOccupied == false) || ignoreOccupied == true)
                            {
                                if (nodeCells[newNeighbourID].getMark() == false) //If not marked
                                {
                                   pathNodes.Add(nodeCells[newNeighbourID]);
                                }
                            }
                        }
                    }
                }
            }
            return pathNodes;
        }

        //Calculates the direct distance between two nodes (ignore obstacles)
        public int findManhattanDistance(NodeCell node1, NodeCell node2)
        {
            int distanceFromEndX = Math.Abs(1 * //1 represents energy required to pass through 
                (int)((node1.getNodeID() % mapSizeX) - (node2.getNodeID() % mapSizeX)));
            int distanceFromEndY = Math.Abs(1 *
                (int)((node1.getNodeID() / mapSizeZ) - (node2.getNodeID() / mapSizeZ)));

            int manhattanDistance = (int)(distanceFromEndX + distanceFromEndY);
            return manhattanDistance;
        }

        //Sets the path based on parentNodes (Child -> Parent -> Parent2 -> ... -> ParentX = pathway)
        public void setPath(NodeCell lastNode)
        {
            for (int i = 0; i < nodeCells.Count; i++)
            {
                nodeCells[i].setMark(false);
            }
            List<NodeCell> nodeList = new List<NodeCell>();
            while (lastNode != nodeCells[startNodeID])
            {
                lastNode = lastNode.getParent();
                nodeList.Add(lastNode);
            }
            
            pathway = nodeList;
        }

        //Unused
        //Remove node from list
        public void removeNode(NodeCell nodeToRemove, List<NodeCell> nodeList)
        {
            for (int count = 0; count < nodeList.Count; count++)
            {
                if (nodeList[count] == nodeToRemove)
                {
                    openList.RemoveAt(count);
                }
            }
        }

        //Rearranges the nodes of a list based on their distanceTotal(Distance moved + distance left) value
        public List<NodeCell> sortNodes(List<NodeCell> nodeList)
        {
            List<NodeCell> sortedList = new List<NodeCell>();
            List<bool> boolList = new List<bool>();
            List<int> nodefValueList = new List<int>();
            for (int i = 0; i < nodeList.Count; i++)
            {
                nodefValueList.Add(nodeList[i].f());
                boolList.Add(false);
            }
            nodefValueList.Sort(); //F values of nodes sorted in ascending order
            int j = 0;
            while (sortedList.Count != nodeList.Count) //Keep adding until sortedList is full
            {
                for (int i = 0; i < nodeList.Count; i++)
                {
                    if (nodefValueList[j] == nodeList[i].f()//If the f value in list matches f value of current node checked
                        && boolList[i] == false)//And node not used before
                    {
                        sortedList.Add(nodeList[i]); //Add node to sorted list
                        boolList[i] = true;
                        j++; //Now checks for next f value in line
                        break;
                    }
                }
            }
            return sortedList;
        }

        //Sets values for ID and List for nodeCells
        public void setValues(int startID, List<NodeCell> nodeCellList)
        {
            startNodeID = startID;
            nodeCells = nodeCellList;
        }

        //Chamge all values back to 0 to be ready for the next pathFind
        public void resetValues()
        {
            openList = new List<NodeCell>();
            closedList = new List<NodeCell>();
            pathway = new List<NodeCell>();

            for (int i = 0; i < nodeCells.Count; i++)
            {
                nodeCells[i].updateDistance(0, 0);
            }
        }

        public List<NodeCell> getPath()
        {
            return pathway;
        }

        //Sets map size
        public void setMapSize(Vector2 mapSize)
        {
            mapSizeX = (int)mapSize.X;
            mapSizeZ = (int)mapSize.Y;
        }

        //Retrieve value of crossing node
        public int getNodeCost(int nodeID)
        {
            return nodeCells[nodeID].getNodeValue();
        }

        //Retrieve value of travelling a path
        public int getDistanceCost(NodeCell startNode, NodeCell endNode)
        {
            int totalDistance = 0;
            pathFind(startNode, endNode);
            for (int i = 0; i < pathway.Count; i++)
            {
                totalDistance += pathway[i].getNodeValue();
            }

            return totalDistance;
        }

        public void move(NodeCell startNode, NodeCell endNode, Character character)
        {
            //pathFind(startNode, endNode);
            if (character.getAPValue() * 2 > pathway.Count)
            {
                character.setCharPosition(startNode.getNodeID());
            }
        }

        //Gets an array of node coordinates that are x size away from currentNode
        public Vector2[] getRadius(int idStartNode, int radiusSize)
        {
            List<Vector2> vectorList = new List<Vector2>();
            int size = radiusSize;
            int drawOnX = (idStartNode % mapSizeX) - size;
            int drawOnY = (idStartNode / mapSizeZ);
            int increaseX = -1;
            int increaseY = -1;
            for (int i = 0; i < size * 4; i++)
            {
                if (drawOnX < mapSizeX && drawOnX >= 0 && drawOnY < mapSizeZ && drawOnY >= 0)
                {
                    vectorList.Add(new Vector2(drawOnX, drawOnY));
                }
                if (drawOnX == (idStartNode % mapSizeX) - 2 || drawOnX == (idStartNode % mapSizeX) + 2)
                {
                    increaseX = -increaseX;
                }
                if (drawOnY == (idStartNode / mapSizeZ) - 2 || drawOnY == (idStartNode / mapSizeZ) + 2)
                {
                    increaseY = -increaseY;
                }
                drawOnX += increaseX;
                drawOnY += increaseY;
            }

            Vector2[] vectorArray = new Vector2[vectorList.Count];
            for (int i = 0; i < vectorList.Count; i++)
            {
                vectorArray[i] = vectorList[i];
            }
            return vectorArray;
        }

        //Changes value of 2D array to list
        public List<NodeCell> arrayToList(NodeCell[,] nodeArray)
        {
            List<NodeCell> nodeCellList = new List<NodeCell>();
            //Add nodes to List via 2D Array
            for (int j = 0; j < nodeArray.GetLength(1); j++)
            {
                for (int i = 0; i < nodeArray.GetLength(0); i++)
                {
                    nodeCellList.Add(nodeArray[i, j]);
                }
            }

            return nodeCellList;
        }
    }
}
