﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace AStarTest
{
    public class Character
    {
        //Character Data
        //Blahdeblahblah
        bool moveable;
        int APValue;

        //Location/Size Data
        int anchorNodeID;
        int xCoordinate;
        int zCoordinate;
        Vector2 characterSize;

        //Map Data
        int mapSizeX = 5;
        int mapSizeZ = 5;

        public Character(bool canMove)
        {
            characterSize = new Vector2(1,1);
            moveable = canMove;
            APValue = 2; //Movement formula: AP * 2 = MaxMoveDistance
        }

        public Vector2 getCharSize()
        {
            return characterSize;
        }

        public void setCharSize(Vector2 size)
        {
            characterSize = size;
        }

        public int getCharID()
        {
            return anchorNodeID;
        }

        public void setCharPosition(int nodeID)
        {
            anchorNodeID = nodeID;
            xCoordinate = nodeID % mapSizeX;
            zCoordinate = nodeID / mapSizeZ;
        }

        public Vector2 getCoordinates()
        {
            Vector2 coordinates = new Vector2(xCoordinate, zCoordinate);
            return coordinates;
        }

        public void setMapSize(Vector2 mapSize)
        {
            mapSizeX = (int)mapSize.X;
            mapSizeZ = (int)mapSize.Y;
        }

        public void setAPValue(int AP)
        {
            APValue = AP;
        }

        public int getAPValue()
        {
            return APValue;
        }
    }
}
