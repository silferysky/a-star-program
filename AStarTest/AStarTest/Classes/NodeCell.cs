﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace AStarTest
{
    public class NodeCell
    {
        //Node Data
        int nodeID;
        int xCoordinate, zCoordinate;

        //For A*
        NodeCell parentNode;
        bool markedNode;
        bool isStartNode;
        bool isEndNode;

        //Node Data (Space)
        bool nodeOccupied;
        bool walkable;
        int nodePassValue; //To pass through
        bool nodeMarked; //If node already has been used
        Character characterUnit = new Character(true);

        //Travel data
        int distanceMoved; //Units moved so far
        int distanceLeft; //Units left (Direct)
        int distanceTotal; //Units moved + units left

        //Map data
        int mapSizeX = 5;
        int mapSizeZ = 5;

        public NodeCell(int ID)
        {
            nodeID = ID;
            xCoordinate = ID % mapSizeX;
            zCoordinate = ID / mapSizeZ;
            walkable = true;
            nodePassValue = 1;
            nodeOccupied = false;
            nodeMarked = false;
            markedNode = false;
            isStartNode = false;
            isEndNode = false;
            characterUnit = null;
        }

        public NodeCell(int ID, bool walk)
        {
            nodeID = ID;
            xCoordinate = ID % mapSizeX;
            zCoordinate = ID / mapSizeZ;//mapSizeZ - (ID / mapSizeZ);
            walkable = walk;
            nodePassValue = 1;
            nodeOccupied = false;
            markedNode = false;
            isStartNode = false;
            isEndNode = false;
            characterUnit = null;
       }

        public void toggleNodeOccupied()
        {
            nodeOccupied = !nodeOccupied;
        }

        public int getNodeID()
        {
            return nodeID;
        }

        public int getNodeValue()
        {
            return nodePassValue;
        }

        public int g()
        {
            return distanceMoved;
        }

        public int h()
        {
            return distanceLeft;
        }

        public int f()
        {
            return distanceTotal;
        }

        public void updateDistance(int dm, int dl)
        {
            distanceMoved = dm;
            distanceLeft = dl;
            distanceTotal = dm + dl;
        }

        public int getDistance()
        {
            return distanceTotal;
        }

        public void setWalkable(bool canWalk)
        {
            walkable = canWalk;
        }

        public bool getWalkable()
        {
            return walkable;
        }

        public void setMark(bool isMarked)
        {
            nodeMarked = isMarked;
        }

        public bool getMark()
        {
            return nodeMarked;
        }

        public void setOccupied(bool isOccupied)
        {
            nodeOccupied = isOccupied;
            if (isOccupied == false)
            {
                characterUnit = null;
            }
        }

        public void setOccupied(Character character)
        {
            characterUnit = character;
            nodeOccupied = true;
        }

        public bool getOccupied()
        {
            return nodeOccupied;
        }

        public Character getCharacter()
        {
            return characterUnit;
        }

        public Vector2 getCoordinates()
        {
            Vector2 coordinates = new Vector2(xCoordinate, zCoordinate);
            return coordinates;
        }

        public void setNode(int nodeTypeID)
        {
            if (nodeTypeID == 0)
            {
                //Normal Node
                isStartNode = false;
                isEndNode = false;
            }
            else if (nodeTypeID == 1)
            {
                isStartNode = true;
                isEndNode = false;
            }
            else if (nodeTypeID == 2)
            {
                isStartNode = false;
                isEndNode = true;
            }
        }

        public NodeCell getParent()
        {
            return parentNode;
        }

        public void setParent(NodeCell parent)
        {
            parentNode = parent;
        }

        public void setMapSize(Vector2 mapSize)
        {
            mapSizeX = (int)mapSize.X;
            mapSizeZ = (int)mapSize.Y;
        }
    }
}
