﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace AStarTest
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Pathfinder pathfinder = new Pathfinder();

        //Lists
        List<NodeCell> nodeCells; //Node spaces
        NodeCell[,] nodeCellArray;
        List<Character> characterList; //Moveable units
        List<Character> blockCellList; //Blocks
        List<NodeCell> pathway; //A* Path
        List<NodeCell> possibleMoveLocations; //A* Path (No end location)
        List<NodeCell> possibleAttackLocations; //Same as possibleMoveLocations, except 1 size bigger;

        Random randomGen = new Random();

        Texture2D blockedBox, openBox, occupiedBox;
        Texture2D startNode, movementNode, locationNode, attackNode, radiusNode, lastNode, marker;

        int currentNodeID;
        Vector2 currentNodeMovementPosition;
        Vector2 currentNodePosition;
        int startNodeID;
        //Vector2 startNodePosition;
        int lastNodeID;

        int mapSizeX = 5;
        int mapSizeZ = 5;

        //Control stuff
        KeyboardState currentKeyboardState = Keyboard.GetState();
        KeyboardState lastKeyboardState;
        bool movementToggle = false;
        bool newSearch = false;
        bool newLocationScout = false;
        bool drawRadius = false;
        bool displayAttackLocations = false;
        bool displayLastLocation = false;

        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            nodeCells = new List<NodeCell>();
            nodeCellArray = new NodeCell[mapSizeX, mapSizeZ];
            pathway = new List<NodeCell>();
            possibleMoveLocations = new List<NodeCell>();
            possibleAttackLocations = new List<NodeCell>();
            blockCellList = new List<Character>();
            characterList = new List<Character>();

            lastNodeID = -1;

            pathfinder.setMapSize(new Vector2(mapSizeX, mapSizeZ));

            //Creates 2D array and nodes
            int nodeCount = 0;
            for (int j = 0; j < mapSizeZ; j++)
            {
                for (int i = 0; i < mapSizeX; i++)
                {
                    NodeCell newNodeCell = new NodeCell(nodeCount);
                    nodeCellArray[i, j] = newNodeCell;
                    nodeCount++;
                }
            }

            //Converts array to list
            nodeCells = pathfinder.arrayToList(nodeCellArray);

            //Creating Blocks (Unmovable terrain)
            for (int i = 0; i < 5; i++)
            {
                Character newBlockSpace = new Character(false);
                //newBlockSpace.setCharSize(new Vector2(randomGen.Next(1, 2), randomGen.Next(1,2)));
                int randomValue = -1;
                while (randomValue < 0)
                {
                    int newRandomValue = randomGen.Next(0, nodeCells.Count);
                    //If Node is not occupied and is walkable
                    if (nodeCells[newRandomValue].getOccupied() == false && nodeCells[newRandomValue].getWalkable() == true)
                    {
                        newBlockSpace.setCharPosition(newRandomValue);
                        randomValue = newRandomValue;
                        //If is regular block
                        if (newBlockSpace.getCharSize() == new Vector2(1, 1))
                        {
                            nodeCells[newRandomValue].setWalkable(false);
                            nodeCells[newRandomValue].setOccupied(true);
                        }
                        else //If bigger than 1x1
                        {
                            
                        }
                    }
                }
                blockCellList.Add(newBlockSpace);
            }

            //Creating characters
            //Current bug: Need to fix spawning (Big units can spawn on top of unmovable terrain)
            for (int i = 0; i < 3; i++)
            {
                Character newCharacter = new Character(true);
                //newCharacter.setCharSize(new Vector2(2, 2));
                int randomValue = -1;
                while (randomValue < 0)
                {
                    int newRandomValue = randomGen.Next(0, nodeCells.Count);//0 to max nodeId value
                    if (nodeCells[newRandomValue].getOccupied() == false)//Node is not occupied
                    {
                        newCharacter.setCharPosition(newRandomValue);
                        randomValue = newRandomValue;

                        if (newCharacter.getCharSize() == new Vector2(1, 1))//Is regular size character
                        {
                            //startNodePosition = new Vector2(newRandomValue % 5, newRandomValue / 5);
                            nodeCells[newRandomValue].setOccupied(true);
                        }
                        else
                        {
                            //1 represents X size (xSize -1), 5 represents Z size ((ySize - 1)* 5)
                            nodeCells[newRandomValue].setOccupied(true);
                            nodeCells[newRandomValue + 1].setOccupied(true);
                            nodeCells[newRandomValue + mapSizeZ].setOccupied(true);
                            nodeCells[newRandomValue + 1 + 10].setOccupied(true);
                            
                            // if (((newCharacter.getCharID() % 5) + xSize < 5) && //5 represent X axis
                              //   ((newCharacter.getCharID() /5) + ySize < 5)) //5 represent Y axis

                        }// End if character size != 1x1
                    }// End If Occupied
                }// End While Loop
                characterList.Add(newCharacter);
            }
            startNodeID = characterList[0].getCharID();

            //Set start node at character start point
            //If more than one, need to specify (change the 0)
            nodeCells[characterList[0].getCharID()].setNode(1);
            currentNodeID = characterList[0].getCharID();
            currentNodePosition = characterList[0].getCoordinates();
            currentNodeMovementPosition = characterList[0].getCoordinates();
            
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            blockedBox = this.Content.Load<Texture2D>("BlackBox");
            openBox = this.Content.Load<Texture2D>("OpenBox");
            occupiedBox = this.Content.Load<Texture2D>("OccupiedBox");
            startNode = this.Content.Load<Texture2D>("StartNode");
            movementNode = this.Content.Load<Texture2D>("MovementBox");
            radiusNode = this.Content.Load<Texture2D>("RadiusBox");
            locationNode = this.Content.Load<Texture2D>("LocationBox");
            attackNode = this.Content.Load<Texture2D>("AttackBox");
            lastNode = this.Content.Load<Texture2D>("EndNode");
            marker = this.Content.Load<Texture2D>("marker");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            lastKeyboardState = currentKeyboardState;
            currentKeyboardState = Keyboard.GetState();

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            
            //Controls:
            //Space to enable/disable movement (Space to start moving, space to stop moving)
            //UpDownLeftRight to control movement
            //Tab to draw circle around node
            //Left shift to display possible attack locations
            //Left Ctrl to display previous location of character

            if (movementToggle)
            {
                if (currentKeyboardState.IsKeyDown(Keys.Left) && lastKeyboardState.IsKeyUp(Keys.Left)
                    && currentNodeID % mapSizeX > 0)
                {
                    currentNodeID -= 1;
                    currentNodePosition = new Vector2(currentNodeID % mapSizeX, currentNodeID / mapSizeZ);
                }
                if (currentKeyboardState.IsKeyDown(Keys.Right) && lastKeyboardState.IsKeyUp(Keys.Right)
                    && currentNodeID % mapSizeX < (mapSizeX -1))
                {
                    currentNodeID += 1;
                    currentNodePosition = new Vector2(currentNodeID % mapSizeX, currentNodeID / mapSizeZ);
                }
                if (currentKeyboardState.IsKeyDown(Keys.Up) && lastKeyboardState.IsKeyUp(Keys.Up)
                     && currentNodeID / mapSizeZ > 0)
                {
                    currentNodeID -= mapSizeX;
                    currentNodePosition = new Vector2(currentNodeID % mapSizeX, currentNodeID / mapSizeZ);
                }
                if (currentKeyboardState.IsKeyDown(Keys.Down) && lastKeyboardState.IsKeyUp(Keys.Down)
                     && currentNodeID / mapSizeZ < (mapSizeZ - 1))
                {
                    currentNodeID += mapSizeX;
                    currentNodePosition = new Vector2(currentNodeID % mapSizeX, currentNodeID / mapSizeZ);
                }
            }

            if (currentKeyboardState.IsKeyDown(Keys.Space) && lastKeyboardState.IsKeyUp(Keys.Space))
            {
                pathfinder.setValues(startNodeID, nodeCells);
                if (!movementToggle)
                    newLocationScout = true;
                else if (movementToggle && nodeCells[currentNodeID].getOccupied() == false)
                {
                    newSearch = true;
                    possibleMoveLocations = new List<NodeCell>(); //Reset upon moving
                }
                movementToggle = !movementToggle;
            }

            if (currentKeyboardState.IsKeyDown(Keys.Tab) && lastKeyboardState.IsKeyUp(Keys.Tab))
            {
                drawRadius = !drawRadius;
            }

            if (currentKeyboardState.IsKeyDown(Keys.LeftShift) && lastKeyboardState.IsKeyUp(Keys.LeftShift))
            {
                displayAttackLocations = !displayAttackLocations;
            }

            if (currentKeyboardState.IsKeyDown(Keys.LeftControl) && lastKeyboardState.IsKeyUp(Keys.LeftControl))
            {
                displayLastLocation = !displayLastLocation;
            }

            if (currentNodeMovementPosition.X < currentNodePosition.X - 0.025f)
            {
                currentNodeMovementPosition.X += 0.02f;
            }
            else if (currentNodeMovementPosition.X > currentNodePosition.X + 0.025f)
            {
                currentNodeMovementPosition.X -= 0.02f;
            }
            else if (currentNodeMovementPosition.Y < currentNodePosition.Y - 0.025f)
            {
                currentNodeMovementPosition.Y += 0.02f;
            }
            else if (currentNodeMovementPosition.Y > currentNodePosition.Y + 0.025f)
            {
                currentNodeMovementPosition.Y -= 0.02f;
            }

            if (newLocationScout == true)
            {
                possibleMoveLocations = pathfinder.getPossibleMoveLocations(nodeCells[startNodeID], characterList[0]);
                possibleAttackLocations = pathfinder.getPossibleAttackLocations(nodeCells[startNodeID], characterList[0]);
                newLocationScout = false;
            }
            if (newSearch == true)
            {
                //Movement Code (May shift part to Pathfinder.cs, depending on preference)
                lastNodeID = startNodeID;
                pathfinder.pathFind(nodeCells[startNodeID], nodeCells[currentNodeID]);
                pathway = pathfinder.getPath();
                if (pathway.Count > 0 && pathway.Count < characterList[0].getAPValue() * 2 + 1) //If path exists
                {
                    nodeCells[startNodeID].setOccupied(false);
                    pathfinder.move(nodeCells[startNodeID], nodeCells[currentNodeID], characterList[0]);
                    startNodeID = currentNodeID;
                    //startNodePosition = currentNodePosition;
                    nodeCells[startNodeID].setOccupied(characterList[0]);
                }
                else
                {
                    pathway = new List<NodeCell>();
                }
                newSearch = false;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            for (int i = 0; i < nodeCells.Count; i++)
            {
                if (nodeCells[i].getWalkable() != true)
                {
                    spriteBatch.Draw(blockedBox,
                    nodeCells[i].getCoordinates() * 100, Color.White);
                }
                else if (nodeCells[i].getOccupied())
                {
                    spriteBatch.Draw(occupiedBox,
                    nodeCells[i].getCoordinates() * 100, Color.White);
                }
                else
                {
                    spriteBatch.Draw(openBox,
                    nodeCells[i].getCoordinates() * 100, Color.White);
                }
            }

            if (movementToggle)
            {
                for (int i = 0; i < possibleMoveLocations.Count; i++)
                {
                    spriteBatch.Draw(locationNode, possibleMoveLocations[i].getCoordinates() * 100, Color.White);
                }
            }

            if (displayAttackLocations && movementToggle)
            {
                for (int i = 0; i < possibleAttackLocations.Count; i++)
                {
                    spriteBatch.Draw(attackNode, possibleAttackLocations[i].getCoordinates() * 100, Color.White);
                }
            }

            for (int i = 0; i < pathway.Count; i++)
            {
                spriteBatch.Draw(movementNode, nodeCells[pathway[i].getNodeID()].getCoordinates() * 100, Color.White);
            }

            if (drawRadius)
            {
                Vector2[] vecArray = pathfinder.getRadius(startNodeID, 2);
                for (int i = 0; i < vecArray.Length; i++)
                {
                    spriteBatch.Draw(radiusNode, vecArray[i] * 100, Color.White);
                }
            }

            if (lastNodeID >= 0 && displayLastLocation)
            {
                spriteBatch.Draw(lastNode, nodeCells[lastNodeID].getCoordinates() * 100, Color.White);
            }

            if (movementToggle)
            {
                spriteBatch.Draw(marker, currentNodeMovementPosition * 100 + new Vector2(25, 25), Color.White);
            }
            else
            {
                spriteBatch.Draw(startNode, currentNodePosition * 100, Color.White);
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
